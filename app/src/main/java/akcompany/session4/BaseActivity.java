package akcompany.session4;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    Context mContext = this;
    Activity mActivity = this;
}
