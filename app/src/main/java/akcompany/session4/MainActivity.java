package akcompany.session4;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

public class MainActivity extends AppCompatActivity {

    TextView txtDate, txtNumber;
    Information information;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();

        checkPermission();
        }

    @Override
    protected void onResume() {
        super.onResume();
        getHawkData();

    }

    public void bind() {
        txtDate = findViewById(R.id.Date);
        txtNumber = findViewById(R.id.Number);
    }

    public void getHawkData() {
        information = Hawk.get("phoneInfo", null);

        if (information != null) {
            txtDate.setText(information.getDateTime());
            txtNumber.setText(information.getPhoneNumber());
        }
    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        1000);
            }
        }


    }

}
